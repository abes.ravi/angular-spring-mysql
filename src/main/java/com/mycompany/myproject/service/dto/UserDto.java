package com.mycompany.myproject.service.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.dozer.Mapping;

public class UserDto {

    @Mapping("id")
    private Long id;

    @Mapping("userName")
    private String firstName;

    @Mapping("email")
    private String email;

    @Mapping("userid")
    private String userid;

    @Mapping("password")
    private String password;

    @Mapping("birthDate")
    private Date burthDate;
    
    @Mapping("joinDate")
    private Date joinDate;

    @Mapping("authorities")
    private Set<AuthorityDto> authorities = new HashSet<AuthorityDto>();

    @Mapping("role")
    private String role;

    @Mapping("desig")
    private String desig;
    
    @Mapping("team")
    private String team;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBurthDate() {
        return burthDate;
    }

    public void setBurthDate(Date burthDate) {
        this.burthDate = burthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<AuthorityDto> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<AuthorityDto> authorities) {
        this.authorities = authorities;
    }

    public String getAuthoritiesAsString() {
        StringBuffer sb = new StringBuffer();
        for (AuthorityDto a : this.getAuthorities()) {
            sb.append(a.getName());
            sb.append(", ");
        }
        return StringUtils.substring(sb.toString(), 0, sb.length() - 2);
    }

}
