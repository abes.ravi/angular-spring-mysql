package com.mycompany.myproject.service.dto;

import java.sql.Date;

import org.dozer.Mapping;

public class EventDto {

    @Mapping("eventId")
    private Long eventId;

    @Mapping("title")
    private String title;

    @Mapping("description")
    private String description;

    @Mapping("startAt")
    private Date startAt;
    
    @Mapping("endAt")
    private Date endAt;

    @Mapping("isFullDay")
    private Boolean isFullDay;

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStartAt() {
		return startAt;
	}

	public void setStartAt(Date startAt) {
		this.startAt = startAt;
	}

	public Date getEndAt() {
		return endAt;
	}

	public void setEndAt(Date endAt) {
		this.endAt = endAt;
	}

	public Boolean getIsFullDay() {
		return isFullDay;
	}

	public void setIsFullDay(Boolean isFullDay) {
		this.isFullDay = isFullDay;
	}


}
