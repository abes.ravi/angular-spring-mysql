package com.mycompany.myproject.service;
import javax.mail.MessagingException;

import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;

import com.mycompany.myproject.model.EmailBody;

public interface EmailService {
  
    public void sendSimpleMessage(SimpleMailMessage message);
    public void sendSimpleMessageWithAttachment(EmailBody email,FileSystemResource file)throws MessagingException;
}