package com.mycompany.myproject.service;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.mycompany.myproject.model.EmailBody;

@Service
public class EmailServiceImpl implements EmailService{
  
    @Autowired
    public JavaMailSenderImpl emailSender;
 
    @Override
    public void sendSimpleMessage(SimpleMailMessage message) {
        emailSender.send(message);
    }
    @Override
    public void sendSimpleMessageWithAttachment(EmailBody email,FileSystemResource file) throws MessagingException {
    	MimeMessage mimeMessage=emailSender.createMimeMessage();
    	MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
    	helper.setTo(email.getTo()); 
    	helper.setSubject(email.getSubject()); 
    	helper.setText(email.getText());
    	helper.addAttachment(file.getFilename(), file);
        emailSender.send(mimeMessage);
    }
}