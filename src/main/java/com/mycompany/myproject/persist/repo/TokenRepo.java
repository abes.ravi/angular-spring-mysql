package com.mycompany.myproject.persist.repo;

import org.springframework.data.repository.CrudRepository;

import com.mycompany.myproject.persist.entity.Token;

public interface TokenRepo extends CrudRepository<Token, String> {
}
