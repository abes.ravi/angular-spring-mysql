package com.mycompany.myproject.persist.repo;

import org.springframework.data.repository.CrudRepository;

import com.mycompany.myproject.persist.entity.Authority;

public interface AuthorityRepo extends CrudRepository<Authority, Long> {
	Authority findById(Long id);
}