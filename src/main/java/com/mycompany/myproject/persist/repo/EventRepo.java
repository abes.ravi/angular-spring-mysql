package com.mycompany.myproject.persist.repo;

import org.springframework.data.repository.CrudRepository;

import com.mycompany.myproject.persist.entity.Event;


public interface EventRepo extends CrudRepository<Event, Long> {


}
