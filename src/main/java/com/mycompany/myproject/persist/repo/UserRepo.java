package com.mycompany.myproject.persist.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mycompany.myproject.persist.entity.User;


public interface UserRepo extends CrudRepository<User, Long> {
    User findByUserid(String login);
    User findByEmail(String email);
    @Query("select max(id) from com.mycompany.myproject.persist.entity.User")
    long getMaxId();

}
