package com.mycompany.myproject.web.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myproject.persist.entity.Authority;
import com.mycompany.myproject.persist.entity.User;
import com.mycompany.myproject.persist.repo.AuthorityRepo;
import com.mycompany.myproject.persist.repo.UserRepo;

import io.swagger.annotations.Api;

@RestController
@Api(description = "Users management API")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private AuthorityRepo authorityRepo;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public @ResponseBody List<User> usersList() {
        logger.debug("get users list");
        return (List<User>) userRepo.findAll();
    }

    @RequestMapping(value = "/users/{userId}", method = RequestMethod.GET)
    public @ResponseBody User getUser(@PathVariable Long userId) {
        logger.debug("get user");
        return userRepo.findOne(userId);
    }

    @RequestMapping(value = "/saveusers", method = RequestMethod.POST)
    @Transactional
    public @ResponseBody User saveUser(@RequestBody User user) {
        logger.debug("save user");
        long id=userRepo.getMaxId();
        user.setId(id+1);
        Set<Authority> authorities = new HashSet<Authority>();
        authorities.add(authorityRepo.findById(Long.valueOf(user.getRole())));
        user.setAuthorities(authorities);
        user=userRepo.save(user);
        return user;
    }

    @RequestMapping(value = "/updatefrgtpwddtl", method = RequestMethod.PUT)
    public @ResponseBody User updateFrgtPwdDtl(@RequestBody User user) {
    	logger.debug("update user");
        userRepo.save(user);
        return user;
    }
    
    @RequestMapping(value = "/updatefrgtpwddtl/{email:.+}", method = RequestMethod.GET)
    public @ResponseBody User getUser(@PathVariable String email) {
        logger.debug("get user");
        return userRepo.findByEmail(email);
    }
}

 
