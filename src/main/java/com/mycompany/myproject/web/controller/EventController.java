package com.mycompany.myproject.web.controller;

import com.mycompany.myproject.persist.entity.Event;
import com.mycompany.myproject.persist.entity.User;
import com.mycompany.myproject.persist.repo.EventRepo;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(description = "Events management API")
public class EventController {

    private static final Logger logger = LoggerFactory.getLogger(EventController.class);

    @Autowired
    private EventRepo eventRepo;

    @RequestMapping(value = "/events", method = RequestMethod.GET)
    public @ResponseBody List<Event> EventsList() {
        logger.debug("get event list");
        return (List<Event>) eventRepo.findAll();
    }
    
    @RequestMapping(value = "/events", method = RequestMethod.POST)
    public @ResponseBody Event saveEvent(@RequestBody Event event) {
        logger.debug("save event");
        eventRepo.save(event);
        return event;
    }
/*
    @RequestMapping(value = "/Events/{EventId}", method = RequestMethod.GET)
    public @ResponseBody Event getEvent(@PathVariable Long EventId) {
        logger.debug("get Event");
        return eventRepo.findOne(EventId);
    }

    @RequestMapping(value = "/Events", method = RequestMethod.POST)
    public @ResponseBody Event saveEvent(@RequestBody Event Event) {
        logger.debug("save Event");
        eventRepo.save(Event);
        return Event;
    }

    @RequestMapping(value = "/updatefrgtpwddtl", method = RequestMethod.PUT)
    public @ResponseBody Event updateFrgtPwdDtl(@RequestBody Event Event) {
    	logger.debug("update Event");
        eventRepo.save(Event);
        return Event;
    }
    
    @RequestMapping(value = "/updatefrgtpwddtl/{email:.+}", method = RequestMethod.GET)
    public @ResponseBody Event getEvent(@PathVariable String email) {
        logger.debug("get Event");
        return eventRepo.findByEmail(email);
    }*/
}

 
