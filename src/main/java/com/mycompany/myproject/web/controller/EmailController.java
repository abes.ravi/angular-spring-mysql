package com.mycompany.myproject.web.controller;
import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.myproject.model.EmailBody;
import com.mycompany.myproject.service.EmailService;

import io.swagger.annotations.Api;

@RestController
@Api(description = "Emails management API")
public class EmailController {
  
    @Autowired
    public EmailService emailService;
 
    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
    public @ResponseBody EmailBody sendSimpleMessage(@RequestBody EmailBody email) {
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(email.getTo()); 
        message.setSubject(email.getSubject()); 
        message.setText(email.getText());
        try
        {
        emailService.sendSimpleMessage(message);
        return email;
        }
        catch(Exception sfe){
        	sfe.printStackTrace();
        }
        return null;
    }
    
    @RequestMapping(value = "/sendEmail/attach", method = RequestMethod.POST)
    public @ResponseBody EmailBody sendSimpleMessageWithAttachment(@RequestBody EmailBody email) {
        try
        {
         FileSystemResource file = new FileSystemResource(new File(email.getAttachmentPath()));
         emailService.sendSimpleMessageWithAttachment(email,file);
        return email;
        }
        catch(Exception sfe){
        	sfe.printStackTrace();
        }
        return null;
    }
}