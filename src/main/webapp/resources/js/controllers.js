'use strict';


myapp.controller('LoginController', function ($rootScope, $scope, AuthSharedService) {
        //$scope.rememberMe = true;
        $scope.login = function () {
        	var mainCaptcha = $scope.mainCaptcha.split(' ').join('');
        	if($scope.captchaInput==undefined){
        		alert("Enter Captcha Correctly");
        	}
        	else
        	{
	            var captchaInput = $scope.captchaInput.split(' ').join('');
	            if (mainCaptcha == captchaInput){
		            $rootScope.authenticationError = false;
		            AuthSharedService.login(
		                $scope.username,
		                $scope.password
		               // $scope.rememberMe
		            );
	        	}
	        	else
	        	{
	        		alert("Invalid Captcha");
	        	}
        	}
        };
        $scope.Captcha = function Captcha(){
            var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
            var i;
            for (i=0;i<6;i++){
              var a = alpha[Math.floor(Math.random() * alpha.length)];
              var b = alpha[Math.floor(Math.random() * alpha.length)];
              var c = alpha[Math.floor(Math.random() * alpha.length)];
              var d = alpha[Math.floor(Math.random() * alpha.length)];
              var e = alpha[Math.floor(Math.random() * alpha.length)];
              var f = alpha[Math.floor(Math.random() * alpha.length)];
              var g = alpha[Math.floor(Math.random() * alpha.length)];
             }
           var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
           $scope.mainCaptcha = code
         }
        
    })
    .controller('HomeController', function ($scope, HomeService) {
        $scope.technos = HomeService.getTechno();
    })
    .controller('UsersController', function ($scope, $log, UsersService) {
        $scope.users = UsersService.getAll();
    })
    .controller('ApiDocController', function ($scope) {
        // init form
        $scope.isLoading = false;
        $scope.url = $scope.swaggerUrl = 'v2/api-docs';
        // error management
        $scope.myErrorHandler = function (data, status) {
            console.log('failed to load swagger: ' + status + '   ' + data);
        };

        $scope.infos = false;
    })
    .controller('TokensController', function ($scope, UsersService, TokensService, $q) {

        var browsers = ["Firefox", 'Chrome', 'Trident']

        $q.all([
            UsersService.getAll().$promise,
            TokensService.getAll().$promise
        ]).then(function (data) {
            var users = data[0];
            var tokens = data[1];

            tokens.forEach(function (token) {
                users.forEach(function (user) {
                    if (token.userLogin === user.login) {
                        token.firstName = user.firstName;
                        token.familyName = user.familyName;
                        browsers.forEach(function (browser) {
                            if (token.userAgent.indexOf(browser) > -1) {
                                token.browser = browser;
                            }
                        });
                    }
                });
            });

            $scope.tokens = tokens;
        });


    })
    .controller('LogoutController', function (AuthSharedService) {
        AuthSharedService.logout();
    })
    .controller('CRUDUserController', function ($scope,$http,$location) {
		    	$scope.registerUser = function() {
		    		$scope.user= {
  	                	  "userName": $scope.userName,
	                	  "userid": $scope.userid,
	                	  "password": $scope.password,
	                	  "birthDate": document.getElementById("birthDate").value,
	                	  "joinDate": document.getElementById("joinDate").value,
	                	  "role": $scope.role,
	                	  "email": $scope.email,
	                	  "team": $scope.team,
	                	  "desig": $scope.desig
	                	};
		    		if($scope.password!=$scope.cnfpassword)
	    		    {
	    		    	alert("New Confirm Password do not match");
	    		    }
	    		    else
	    		    {
		    		 $http.post('saveusers', $scope.user)
	                    .success(function (data) {
	                        alert("Registertaion success.Enter new login userid and password.");
	                        $location.path("/login");
	                    })
	                    .error(function (data) {
	                    	if(data.exception=="org.springframework.dao.DataIntegrityViolationException")
	                    		alert("UserId allready exist.");
	                    });
	    		    }
		    	}
    	    	$scope.updatePassword = function() {
    	    		    if($scope.password!=$scope.cnfpassword)
    	    		    {
    	    		    	alert("New Confirm Password do not match");
    	    		    }
    	    		    else
    	    		    {
    	                $scope.user.data.password=$scope.password;
    	                $http.put('updatefrgtpwddtl', $scope.user.data)
    	                    .success(function (data) {
    	                        alert("success");
    	                        $location.path("/login");
    	                    })
    	                    .error(function (data) {
    	                    	alert("failure");
    	                    });
    	    		    }
    	            } 
    	    	$scope.getUserByEmail = function () {$http.get('updatefrgtpwddtl/'+$scope.email)
                    .then(function (data) {
                    	if(!data.data)
                    	alert("Invalid Email Id");
                    	else
                    	$scope.user= data;
                    });
    	    	}
    })
    .controller('EmailController', function ($scope,$http) {
    	    	$scope.sendEmail = function() {
    	                $http.post('sendEmail', {
    	                	  "subject": "hi",
    	                	  "text": "hi",
    	                	  "to": "abes.ravi@gmail.com"
    	                	})
    	                    .success(function (data) {
    	                        alert("success");
    	                    })
    	                    .error(function (data) {
    	                    	alert("failure");
    	                    });
    	            } 
    })
    .controller('EventsController', function ($scope,$http,$location, $log, EventsService,$q) {
	    'use strict';
	    $http.defaults.headers.post["Content-Type"] = "application/json";
	    $scope.changeMode = function (mode) {
	        $scope.mode = mode;
	        if(mode == 'month')
	        	$scope.event=false;
	    };
	
	    $scope.moreData = function () {
	        $scope.items = [ 'Import', 'Export' ];
	        $scope.dropboxitemselected = function (item) {
	            $scope.selectedItem = item;
	            alert($scope.selectedItem);
	        }
	    };
	
	    $scope.today = function () {
	        $scope.currentDate = new Date();
	    };
	
	    $scope.isToday = function () {
	        var today = new Date(),
	            currentCalendarDate = new Date($scope.currentDate);
	
	        today.setHours(0, 0, 0, 0);
	        currentCalendarDate.setHours(0, 0, 0, 0);
	        return today.getTime() === currentCalendarDate.getTime();
	    };
	
	    $scope.loadEvents = function () {
	    	$scope.eventSource = createEvents();
	    };
	    
	    $scope.reloadSource = function (startTime, endTime){
	    	//Events.query({startTime: startTime, endTime: endTime}, function(events){
                //$scope.eventSource=events;
	    	console.log("startTime: "+startTime+" endTime: "+endTime);
	    	$scope.eventSource = createEvents();
	    	//$scope.$broadcast('eventSourceChanged',$scope.eventSource);
	    	// createEvents();
	    	console.log("reloadSource: "+$scope.eventSource);
            //});
	    };
	
	    $scope.onEventSelected = function (event) {
	        $scope.event = event;
	    };
	    
	
	    $scope.onTimeSelected = function (selectedTime, events) {
	        console.log('Selected time: ' + selectedTime + ' hasEvents: ' + (events !== undefined && events.length !== 0));
	    };
	    
	    
	    $scope.quickAdd = function () {
	    	var urlBase="";
	    	var matcher= ['at','@','AM','PM','TODAY','2DAY','TOMORROW','TOMMOROW','TOMOROW'];
	    	if($scope.quickAddInputText =="" || $scope.quickAddInputText.length == 0){
				alert("Insufficient Data! Please try different combination.");
			}
	    	else {
			var tokens=$scope.quickAddInputText.split(' ');
			var day; var eventTime;
			var title="";var hrs;
			var date= new Date();
			var startDay,endDay;
			var startMinute,endMinute;
			for(var x=0;x <tokens.length;x++ ){
				if(tokens[x] == matcher[0] || tokens[x] == matcher[1] || tokens[x].trim().toLowerCase().localeCompare(matcher[0]) == 0 ){
					for(var y=x,z=0; y> 0; y--,z++  )
						title += tokens[z] +' ';
				}
				if(tokens[x].trim().toUpperCase().localeCompare(matcher[2]) == 0 || tokens[x].trim().toUpperCase().localeCompare(matcher[3]) == 0){
					if(tokens[x-1].length > 2){
						var tempTime=tokens[x-1].split(':');
						eventTime=parseInt(tempTime[0]) + parseFloat((tempTime[1]/60));	
					}
					if(tokens[x-1].length <= 2)
						eventTime=parseInt(tokens[x-1]);
					
					if(tokens[x].trim().toUpperCase().localeCompare(matcher[2]) == 0)
						hrs=0;
					else
						hrs=12;
				}
				
				if(tokens[x].trim().length <= 7 
						&& parseInt(tokens[x].trim()) <= 12 
						&& (tokens[x].substr(tokens[x].trim().length-2,2).trim().toUpperCase().localeCompare(matcher[2]) == 0 
								|| tokens[x].substr(tokens[x].trim().length-2,2).trim().toUpperCase().localeCompare(matcher[3]) == 0)
								){
					if(tokens[x].substr(0,tokens[x].trim().length-2).length > 2){
						var tempTime=tokens[x].substr(0,tokens[x].trim().length-2).split(':');
						eventTime=parseInt(tempTime[0]) + parseFloat((tempTime[1]/60));	
					}
					if(tokens[x].substr(0,tokens[x].trim().length-2).length <= 2)
						eventTime=parseInt(tokens[x]);
					if(tokens[x].substr(tokens[x].trim().length-2,2).trim().toUpperCase().localeCompare(matcher[2]) == 0)
						hrs=0;
					else
						hrs=12;
				}
								
				if (tokens[x].trim().toUpperCase().localeCompare(matcher[4]) == 0 || tokens[x].trim().toUpperCase().localeCompare(matcher[5]) == 0)
					startDay=0;
				if (tokens[x].trim().toUpperCase().localeCompare(matcher[6]) == 0 || tokens[x].trim().toUpperCase().localeCompare(matcher[7]) == 0 || tokens[x].trim().toUpperCase().localeCompare(matcher[8]) == 0)
					startDay=1;
			}
			var description=title;

			for(var i=0;i<tokens.length;i++)
				console.log(tokens[i]);
			startMinute=(Number(eventTime)+Number(hrs))*60;
			
			var startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0,  startMinute);
            var endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0,  startMinute + 60);
		   // for(var i=0;i<tokens.length;i++)
		    		//Dinner with team at 7pm yesterday
            //http://localhost:8080//events  http://localhost:8080/index.html#/events
            console.log("urlBase: "+urlBase+" startDay: "+startDay+" startMinute: "+startMinute+ " startTime: "+startTime+" endTime: "+ endTime);
            var events = [];
            events.push({
            	title: title,
            	description: description,
   			 	startTime: startTime,
   			 	endTime: endTime,
   			 	allDay: false
            });
            
            console.log(events);
			 $http.post(urlBase + '/events', {
				 title: title,
				 description: description,
				 startAt: startTime,
				 endAt: endTime,
				 isFullDay: false
	            } ).
			  success(function(data, status, headers) {
				  alert ("Events added in your calendar!");
				 $scope.$broadcast('eventSourceChanged',createEvents());
			    });
			}
	    };
	
	    function createEvents() {
	        var events = [] ;    
	        $q.all([
	            EventsService.getAll().$promise
	        ]).then(function (data) {
	            var users = data[0];
	                users.forEach(function (user) {
	                	var date=new Date();
	                     var eventType = user.isFullDay;
	                     var endTime= null;
	                     if (user.endAt != null || user.endAt != "")
	                    	 endTime=new Date(user.endAt);
	                     //console.log(user);
	                    if (eventType === 0) {
	                        events.push({
	                             title:user.title,
	                             startTime: new Date(user.startAt),
	                             endTime: endTime,
	                             allDay: true
	                         });
	                     } else {
	                         events.push({
	                        	 title:user.title,
	                             startTime: new Date(user.startAt),
	                             endTime: endTime,
	                             allDay: false
	                         });
	                         //console.log(" startTime: "+new Date(user.startAt)+" endTime: "+ new Date(user.endAt));
	                     }
	                    $scope.$broadcast('eventSourceChanged',events);
	             
	                });
	            });
	       
	          return events;
	        
	    }
	    })
     .controller('EmailAttachController', function ($scope,$http) {
    	    	$scope.sendEmailWithAttachment = function() {
    	                $http.post('sendEmail/attach', {
    	                	  "attachmentPath": "g:/36829-n.pdf",
    	                	  "subject": "hi",
    	                	  "text": "hi",
    	                	  "to": "abes.ravi@gmail.com"
    	                	})
    	                    .success(function (data) {
    	                        alert("success");
    	                    })
    	                    .error(function (data) {
    	                    	alert("failure");
    	                    });
    	            } 
    })
    .controller('ErrorController', function ($scope, $routeParams) {
        $scope.code = $routeParams.code;

        switch ($scope.code) {
            case "403" :
                $scope.message = "Oops! you have come to unauthorised page."
                break;
            case "404" :
                $scope.message = "Page not found."
                break;
            default:
                $scope.code = 500;
                $scope.message = "Oops! unexpected error"
        }

    });